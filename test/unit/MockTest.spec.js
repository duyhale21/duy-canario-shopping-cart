const { isBlackFriday } = require('../../src/config/flagsFunction');

class MockConfigInterface {
    async getValueAsync(key, defaultValue) {
        if (key === 'enableBlackFridaySales') {
            return true
        }
        return defaultValue
    }
}

describe('isBlackFriday', () => {
    // Test case for Black Friday sale enabled
    it('returns true when Black Friday sale is enabled', async () => {
        const configInterface = new MockConfigInterface()

        const result = await isBlackFriday(configInterface)

        expect(result).toBe(true)
    })

    // Test case for Black Friday sale disabled
    it('returns false when Black Friday sale is disabled', async () => {
        // Arrange
        const configInterface = new MockConfigInterface()
        jest.spyOn(configInterface, 'getValueAsync').mockReturnValue(false)

        const result = await isBlackFriday(configInterface);

        expect(result).toBe(false)
    })

    it('returns false and logs error when an error occurs during retrieval', async () => {
        const configInterface = new MockConfigInterface()
        jest.spyOn(configInterface, 'getValueAsync').mockRejectedValue(new Error('Error occurred'))

        const consoleError = jest.spyOn(console, 'error').mockImplementation(() => {})

        const result = await isBlackFriday(configInterface)

        expect(result).toBe(false)
        expect(consoleError).toHaveBeenCalled()

        consoleError.mockRestore()
    });
});
