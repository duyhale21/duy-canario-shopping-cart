import firebase from 'firebase'

const config = {
  apiKey: "AIzaSyAY0l-OARQCZIKKERvu2JyHFu-oVdqZkqo",
  authDomain: "duy-canario.firebaseapp.com",
  databaseURL: "https://duy-canario-default-rtdb.europe-west1.firebasedatabase.app",
}

firebase.initializeApp(config);

export function firebaseListener(func) {
  firebase.auth().onAuthStateChanged(function(user) {
    if (user) {
      console.log("User log in success", user);
      func(true, user)
    } else {
      console.log("User log in failed", user);
      func(false)
    }
  }, function(error) {
    console.log(error)
  });
}

export const ref = firebase.database().ref();
export const firebaseAuth = firebase.auth;
