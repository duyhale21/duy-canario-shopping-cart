import * as configCat from "configcat-js";
import { isBlackFriday } from "./flagsFunction";

export const featureToggles = {
    BLACK_FRIDAY_SALE: false
};

isBlackFriday(configCat.getClient("1jbcCI0eB0iLU4RtOIYp5Q/sm9kRuFykkCrN_XiqD8YpQ")).then((res) => {
    featureToggles.BLACK_FRIDAY_SALE = res
})

