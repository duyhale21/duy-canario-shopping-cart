async function isBlackFriday(configInterface) {
      try {
          return await configInterface.getValueAsync("enableBlackFridaySales", false);
      } catch (err) {
          console.error(err);
          return false
      }
}

module.exports = { isBlackFriday };